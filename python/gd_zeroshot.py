import torch
import torch.nn as nn
import torch.optim as optim
from torchvision import datasets, transforms
from torch.autograd import Variable
from torch.utils.data import Dataset, DataLoader
import torch.nn.functional as F
import numpy as np
import random
import json
import matplotlib.pyplot as plt
import pylab

batch_size = 16
learning_rate = 0.0001

train_ents = set()
with open('gd/all_entities.txt') as ents_file:
    for line in ents_file.readlines():
        train_ents.add(line.strip())

d_ents = set()
with open('gd/d_entities.txt') as dents_file:
    for line in dents_file.readlines():
        d_ents.add(line.strip())

zero = True
input_e = 'gd/gdvec_P.txt'

proteins = {}
with open(input_e) as vector_file:
    vectors = vector_file.readlines()
    for line in vectors:
        args = line.split(' ')
        vec = None
        if args[0] in train_ents:
            if args[0] in d_ents and zero:
                vec = np.random.randn(200)
                vec /= np.sqrt(np.sum(vec ** 2))
            else:
                vec = np.empty(200, np.float)
                for i in range(0, 200):
                    vec[i] = float(args[i + 1])
            proteins[args[0]] = vec


"""
labels = []
with open('ppi_human_positive_shuffled.txt') as pos_file:
    i = 0
    while i < 50500:
        ls = pos_file.readline().strip().split(' ')
        if ls[0] in proteins and ls[1] in proteins:
            labels.append([ls[0], ls[1], '1'])
            i += 1
with open('ppi_human_negative_shuffled.txt') as neg_file:
    i = 0
    while i < 50500:
        ls = neg_file.readline().strip().split(' ')
        if ls[0] in proteins and ls[1] in proteins:
            labels.append([ls[0], ls[1], '0'])
            i += 1
random.shuffle(labels)


class TrainSet(Dataset):
    def __init__(self):
        self.length = 100000

    def __len__(self):
        return self.length

    def __getitem__(self, item):
        v = labels[item]
        return np.concatenate((proteins[v[0]], proteins[v[1]])), int(v[2])


class TestSet(Dataset):
    def __init__(self):
        self.length = 1000

    def __len__(self):
        return self.length

    def __getitem__(self, item):
        v = labels[30000 + item]
        return np.concatenate((proteins[v[0]], proteins[v[1]])), int(v[2])


class ValidateSet(Dataset):
    def __init__(self):
        self.length = 1000

    def __len__(self):
        return self.length

    def __getitem__(self, item):
        v = labels[100000 + item]
        return np.concatenate((proteins[v[0]], proteins[v[1]])), int(v[2])


train_loader = DataLoader(dataset=TrainSet(), batch_size=batch_size, shuffle=True)
test_loader = DataLoader(dataset=TestSet(), batch_size=batch_size, shuffle=False)
validate_loader = DataLoader(dataset=ValidateSet(), batch_size=batch_size, shuffle=False)
"""


class PPISet(Dataset):
    def __init__(self, fname):
        self.labels = []
        with open(fname, 'r') as fin:
            lines = fin.readlines()
            for line in lines:
                ls = line.strip().split('\t')
                if ls[0] in proteins and ls[1] in proteins:
                    self.labels.append((ls[0], ls[1], ls[2]))

    def __len__(self):
        return len(self.labels)

    def __getitem__(self, item):
        v = self.labels[item]
        return np.concatenate((proteins[v[0]], proteins[v[1]])), int(v[2])


train_loader = DataLoader(dataset=PPISet('gd/gd_train.txt'), batch_size=batch_size, shuffle=True)
validate_loader = DataLoader(dataset=PPISet('gd/gd_validate.txt'), batch_size=batch_size, shuffle=False)
test1_loader = DataLoader(dataset=PPISet('gd/gd_testee.txt'), batch_size=batch_size, shuffle=False)
test2_loader = DataLoader(dataset=PPISet('gd/gd_tested.txt'), batch_size=batch_size, shuffle=False)
test3_loader = DataLoader(dataset=PPISet('gd/gd_testdd.txt'), batch_size=batch_size, shuffle=False)

class Net(nn.Module):

    def __init__(self):
        super(Net, self).__init__()

        self.classifier = nn.Sequential(
            nn.Linear(400, 256),
            nn.ReLU(True),
            #nn.Dropout(),
            nn.Linear(256, 256),
            nn.ReLU(True),
            #nn.Dropout(),
            nn.Linear(256, 2))

    def forward(self, x):
        return self.classifier(x)


model = Net()

criterion = nn.CrossEntropyLoss()
#optimizer = optim.SGD(model.parameters(), lr=0.001, momentum=0.9)
optimizer = optim.Adam(model.parameters(), lr=learning_rate)

train_loss_curve = []
test_loss_curve = []
validate_loss_curve = []
auc_data = np.zeros([len(train_loader.dataset)], np.dtype([("pred", float), ("targ", int)]))

def calc_prob(v):
    v = 1 / (1 + np.exp(-v))
    return v[1] / (v[0] + v[1])

def train(epoch):
    model.train()
    correct = 0
    i = 0
    global auc_data
    TP, TN, FN, FP = 0., 0., 0., 0.
    pcnt, ncnt = 0., 0.
    train_loss = 0
    for batch_idx, (data, target) in enumerate(train_loader):
        data, target = Variable(data).float(), Variable(target)
        optimizer.zero_grad()
        output = model(data)
        loss = criterion(output, target)
        train_loss += loss.data
        loss.backward()
        optimizer.step()
        pred = output.data.max(1, keepdim=True)[1]

        correct += pred.eq(target.data.view_as(pred)).cpu().sum()
        target = target.data.view_as(pred)
        TP += ((pred == 1) & (target == 1)).cpu().sum()
        TN += ((pred == 0) & (target == 0)).cpu().sum()
        FN += ((pred == 0) & (target == 1)).cpu().sum()
        FP += ((pred == 1) & (target == 0)).cpu().sum()
        pcnt += (target == 1).cpu().sum()
        ncnt += (target == 0).cpu().sum()

        for j in range(len(target)):
            v = np.array(output[j].detach())
            auc_data[i][0] = calc_prob(v)
            auc_data[i][1] = int(target[j])
            i += 1
        if batch_idx % 1000 == 0:
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                epoch, batch_idx * len(data), len(train_loader.dataset),
                100. * batch_idx / len(train_loader), loss.data))

    train_loss /= len(train_loader.dataset)
    train_loss_curve.append(train_loss)

    p = TP.float() / (TP + FP)
    r = TP.float() / (TP + FN)
    tpr = TP.float() / (TP + FN)
    fpr = FP.float() / (FP + TN)

    auc_data = np.sort(auc_data, order='pred')
    h = int(pcnt)
    area = 0

    auc_h = []
    for i in range(len(auc_data)):
        if auc_data[i][1] == 1:
            h -= 1
        else:
            area += h
            auc_h.append(h)

    print('Train set: Average loss: {:.8f}, Accuracy: {}/{} ({:.1f}%), Precision: {:.1f}%, Recall: {:.1f}%, F1: {:.1f}%, AUC: {:.4f}'.format(
            train_loss, correct, len(train_loader.dataset),
            100. * float(correct) / len(train_loader.dataset), p * 100, r * 100, 2 * p * r / (p + r) * 100,
            float(area) / int(pcnt) / int(ncnt)))


min_loss = 100000000


def validate(dataloader, draw=False):
    val_auc_data = np.zeros([len(dataloader.dataset)], np.dtype([("pred", float), ("targ", int)]))

    model.eval()
    test_loss = 0
    correct = 0
    TP, TN, FN, FP = 0., 0., 0., 0.
    pcnt, ncnt = 0., 0.
    i = 0

    for data, target in dataloader:
        data, target = Variable(data, volatile=True).float(), Variable(target)
        output = model(data)
        test_loss += criterion(output, target).data
        pred = output.data.max(1, keepdim=True)[1]

        correct += pred.eq(target.data.view_as(pred)).cpu().sum()
        target = target.data.view_as(pred)
        TP += ((pred == 1) & (target == 1)).cpu().sum()
        TN += ((pred == 0) & (target == 0)).cpu().sum()
        FN += ((pred == 0) & (target == 1)).cpu().sum()
        FP += ((pred == 1) & (target == 0)).cpu().sum()
        pcnt += (target == 1).cpu().sum()
        ncnt += (target == 0).cpu().sum()

        for j in range(len(target)):
            v = np.array(output[j].detach())
            val_auc_data[i][0] = calc_prob(v)
            val_auc_data[i][1] = int(target[j])
            i += 1

    p = TP.float() / (TP + FP)
    r = TP.float() / (TP + FN)
    tpr = TP.float() / (TP + FN)
    fpr = FP.float() / (FP + TN)

    val_auc_data = np.sort(val_auc_data, order='pred')
    h = int(pcnt)
    area = 0

    auc_h = []
    for i in range(len(val_auc_data)):
        if val_auc_data[i][1] == 1:
            h -= 1
        else:
            area += h
            auc_h.append(h)

    test_loss /= len(dataloader.dataset)
    if draw:
        validate_loss_curve.append(test_loss)

        plt.subplot(1, 2, 1)
        plt.plot(np.arange(0, ncnt), auc_h)
        x = np.arange(0, len(train_loss_curve))
        plt.subplot(1, 2, 2)
        plt.plot(x, train_loss_curve)
        plt.plot(x, validate_loss_curve)
        plt.draw()
        pylab.show()

    global min_loss
    if min_loss > test_loss:
        min_loss = test_loss
        torch.save(model.state_dict(), 'result.mdl')

    print('Validate set: Average loss: {:.8f}, Accuracy: {}/{} ({:.1f}%), Precision: {:.1f}%, Recall: {:.1f}%, F1: {:.1f}%, AUC: {:.4f}'.format(
        test_loss, correct, len(dataloader.dataset), 100. * float(correct) / len(dataloader.dataset), p * 100, r * 100,
        2 * p * r / (p + r) * 100, float(area) / int(pcnt) / int(ncnt)))


epoch = 1
while True:
    try:
        train(epoch)
        validate(validate_loader, False)
        validate(test1_loader)
        validate(test2_loader)
        validate(test3_loader)
        print()
        epoch += 1
    except KeyboardInterrupt:
        break
model.load_state_dict(torch.load('result.mdl'))
