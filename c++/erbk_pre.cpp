#include <QtCore>

QMap<QString, QSet<QString>> leftent, rightent;

int main(int argc, char *argv[]) {
	QCoreApplication a(argc, argv);

	QFile train("../../DKRL-gd/human/train.txt");
	if (!train.open(QIODevice::ReadOnly))
		return 0;

	QFile tph("../../DKRL-gd/human/tail_per_head.txt");
	if (!tph.open(QIODevice::WriteOnly))
		return 0;

	QFile hpt("../../DKRL-gd/human/head_per_tail.txt");
	if (!hpt.open(QIODevice::WriteOnly))
		return 0;

	while (!train.atEnd()) {
		QStringList args = QString(train.readLine().trimmed()).split('\t');
		leftent[args[2]].insert(args[0]);
		rightent[args[2]].insert(args[1]);
	}

	for (auto ent : leftent.keys()) {
		tph.write((ent + ' ' + QString::number(leftent[ent].size())
			+ ' ' + QString::number(rightent[ent].size())).toLatin1() + '\n');
	}

	return 0;
}
