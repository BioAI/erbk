#include <QtCore>
#include <iostream>
#include <random>
#include <functional>

QString compstr[] = { "Cytoplasm & Nucleus", "Cytoplasm", "Nucleus", "Mitochondrion", "Endoplasmic reticulum", "Golgi apparatus", "Peroxisome" };
std::default_random_engine rnd(time(0));

const int train_size = 50000, test_size = 500, ee_size = train_size + 2 * test_size;
const int train_pros = 2000, test_pros = 200;

template<typename T>
QVector<T> filter(const QVector<T>& v, std::function<bool(T)> f) {
	QVector<T> res;
	for (auto& t : v)
		if (f(t))
			res.push_back(t);
	return res;
}

bool interact(int ca, int cb) {
	if (ca == cb) return true;
	if (cb == 0) std::swap(ca, cb);
	if (ca == 0 && cb < 3) return true;
	return false;
}

void writepair(QFile& file, QPair<QString, QString> p, bool label, bool swap = false) {
	static std::uniform_int_distribution<> r2(0, 1);
	if (swap && r2(rnd))
		std::swap(p.first, p.second);
	file.write((p.first + '\t' + p.second + '\t' + (label ? '1' : '0') + '\n').toLatin1());
}

int main(int argc, char* argv[]) {
	QCoreApplication a(argc, argv);

	QFile link("../../OPA2Vec/4932.protein.links.v11.0.txt"); // 9606
	if (!link.open(QIODevice::ReadOnly))
		return 0;

	QFile map("../../OPA2Vec/4932.protein.info.v11.0.txt");
	if (!map.open(QIODevice::ReadOnly))
		return 0;

	QFile comp("../../DKRL/yeast_component.txt");
	if (!comp.open(QIODevice::ReadOnly))
		return 0;

	QFile gaf("../../DKRL/goa_yeast.gaf");
	if (!gaf.open(QIODevice::ReadOnly))
		return 0;

	QFile fdpros("../../DKRL/d_proteins_yeast.txt");
	if (!fdpros.open(QIODevice::ReadOnly))
		return 0;

	QFile train("../../DKRL/ppi_train.txt");
	if (!train.open(QIODevice::WriteOnly))
		return 0;

	QFile validate("../../DKRL/ppi_validate.txt");
	if (!validate.open(QIODevice::WriteOnly))
		return 0;

	QFile testee("../../DKRL/ppi_testee.txt");
	if (!testee.open(QIODevice::WriteOnly))
		return 0;

	QFile tested("../../DKRL/ppi_tested.txt");
	if (!tested.open(QIODevice::WriteOnly))
		return 0;

	QFile testdd("../../DKRL/ppi_testdd.txt");
	if (!testdd.open(QIODevice::WriteOnly))
		return 0;

	QMap<QString, QString> map1, map2;
	while (!map.atEnd()) {
		QStringList e = QString(map.readLine().trimmed()).split('\t');
		map1[e[0]] = e[1];
	}

	while (!gaf.atEnd()) {
		QStringList e = QString(gaf.readLine().trimmed()).split('\t');
		map2[e[2]] = e[1];
	}

	QVector<QString> prov;
	QSet<QString> pros;
	QMap<QString, int> proloc;
	while (!comp.atEnd()) {
		QStringList e = QString(comp.readLine().trimmed()).split('\t');
		for (int i = 0; i < 7; i++) {
			if (e[1] == compstr[i]) {
				prov.push_back(e[0]);
				pros.insert(e[0]);
				proloc[e[0]] = i;
				break;
			}
		}
	}
	std::shuffle(prov.begin(), prov.end(), rnd);

	QVector<QString> dprov;
	QSet<QString> dpros;
	while (!fdpros.atEnd()) {
		QString pro = fdpros.readLine().trimmed();
		if (pros.contains(pro)) {
			dprov.push_back(pro);
			dpros.insert(pro);
		}
	}
	std::shuffle(dprov.begin(), dprov.end(), rnd);

	QVector<QString> eprov;
	for (auto& p : pros)
		if (!dpros.contains(p))
			eprov.push_back(p);
	std::shuffle(eprov.begin(), eprov.end(), rnd);

	QVector<QPair<QString, QString>> linkv[3];
	QSet<QPair<QString, QString>> links;
	int n = 0;
	while (!link.atEnd()) {
		QStringList e = QString(link.readLine().trimmed()).split(' ');
		if (++n % 1000000 == 0)
			std::cout << n << std::endl;

		e[0] = map2[map1[e[0]]];
		e[1] = map2[map1[e[1]]];
		if (!pros.contains(e[0]) || !pros.contains(e[1]))
			continue;
		auto p = QPair<QString, QString>(e[0], e[1]);
		links.insert(p);
		bool a = dpros.contains(e[0]), b = dpros.contains(e[1]);
		if (a != b)
			linkv[1].push_back(p);
		else if (a)
			linkv[2].push_back(p);
		else linkv[0].push_back(p);
	}

	std::cout << linkv[0].size() << ' ' << linkv[1].size() << ' ' << linkv[2].size() << std::endl;

	QSet<QPair<QString, QString>> eepos, eeneg, edpos, edneg, ddpos, ddneg;

	// =================================================== e-e

	eprov.resize(train_pros);
	QSet<QString> epros;
	for (auto& p : eprov)
		epros.insert(p);
	linkv[0] = filter<QPair<QString, QString>>(linkv[0], [&epros](QPair<QString, QString> p) {
		return epros.contains(p.first) && epros.contains(p.second);
		});

	std::shuffle(linkv[0].begin(), linkv[0].end(), rnd);
	for (int i = 0; i < ee_size; i++)
		eepos.insert(linkv[0][i]);

	std::uniform_int_distribution<> r4000(0, train_pros - 1);
	while (eeneg.size() < ee_size) {
		QString a = eprov[r4000(rnd)];
		QString b = eprov[r4000(rnd)];
		if (a == b) continue;
		QPair<QString, QString> p(a, b), ip(b, a);
		if (eeneg.contains(p) || eeneg.contains(ip)) continue;
		if (links.contains(p) || links.contains(ip)) continue;
		if (interact(proloc[a], proloc[b])) continue;
		eeneg.insert(p);
	}

	// =================================================== d-d

	dprov.resize(test_pros);
	dpros.clear();
	for (auto& p : dprov)
		dpros.insert(p);
	linkv[2] = filter<QPair<QString, QString>>(linkv[2], [&dpros](QPair<QString, QString> p) {
		return dpros.contains(p.first) && dpros.contains(p.second);
		});

	std::shuffle(linkv[2].begin(), linkv[2].end(), rnd);
	for (int i = 0; i < test_size; i++)
		ddpos.insert(linkv[2][i]);

	std::uniform_int_distribution<> r400(0, test_pros - 1);
	while (ddneg.size() < test_size) {
		QString a = dprov[r400(rnd)];
		QString b = dprov[r400(rnd)];
		if (a == b) continue;
		QPair<QString, QString> p(a, b), ip(b, a);
		if (ddneg.contains(p) || ddneg.contains(ip)) continue;
		if (links.contains(p) || links.contains(ip)) continue;
		if (interact(proloc[a], proloc[b])) continue;
		ddneg.insert(p);
	}

	// =================================================== e-d

	eprov.resize(test_pros / 2);
	epros.clear();
	for (auto& p : eprov)
		epros.insert(p);
	dprov.resize(test_pros / 2);
	dpros.clear();
	for (auto& p : dprov)
		dpros.insert(p);
	linkv[1] = filter<QPair<QString, QString>>(linkv[1], [&dpros, &epros](QPair<QString, QString> p) {
		return dpros.contains(p.first) != dpros.contains(p.second)
			&& epros.contains(p.first) != epros.contains(p.second);
		});

	std::shuffle(linkv[1].begin(), linkv[1].end(), rnd);
	for (int i = 0; i < test_size; i++)
		edpos.insert(linkv[1][i]);

	std::uniform_int_distribution<> r200(0, test_pros / 2 - 1);
	while (edneg.size() < test_size) {
		QString a = eprov[r200(rnd)];
		QString b = dprov[r200(rnd)];
		QPair<QString, QString> p(a, b), ip(b, a);
		if (edneg.contains(p) || edneg.contains(ip)) continue;
		if (links.contains(p) || links.contains(ip)) continue;
		if (interact(proloc[a], proloc[b])) continue;
		edneg.insert(p);
	}

	// ===================================================

	int ct = 0;
	for (auto& p : eepos) {
		if (ct < train_size) writepair(train, p, 1);
		else if (ct < train_size + test_size) writepair(validate, p, 1);
		else writepair(testee, p, 1);
		ct++;
	}
	ct = 0;
	for (auto& p : eeneg) {
		if (ct < train_size) writepair(train, p, 0);
		else if (ct < train_size + test_size) writepair(validate, p, 0);
		else writepair(testee, p, 0);
		ct++;
	}
	for (auto& p : edpos)
		writepair(tested, p, 1, true);
	for (auto& p : edneg)
		writepair(tested, p, 0, true);
	for (auto& p : ddpos)
		writepair(testdd, p, 1);
	for (auto& p : ddneg)
		writepair(testdd, p, 0);

	return 0;
}
