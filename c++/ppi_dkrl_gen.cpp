#include <QtCore>

QMap<QString, QString> prop_names;
QSet<QString> entities, relations;
QMap<QString, int> words;
QMap<QString, QStringList> entity_desc;
QMap<QString, QString> prot_func;
QSet<QPair<QString, QString>> annotations;
QMap<QString, QString> cates;
QSet<QString> stopwords;

QString remove_punc(QString Str) {
	Str = Str.toLower().trimmed();
	for (QChar &c : Str) {
		if (!(c >= 'a' && c <= 'z'))
			c = ' ';
	}
	return Str;
}

int main(int argc, char *argv[]) {
	QCoreApplication a(argc, argv);

	QFile train("../../DKRL/train.txt");
	if (!train.open(QIODevice::WriteOnly))
		return 0;

	QFile entity2id("../../DKRL/entity2id.txt");
	if (!entity2id.open(QIODevice::WriteOnly))
		return 0;

	QFile relation2id("../../DKRL/relation2id.txt");
	if (!relation2id.open(QIODevice::WriteOnly))
		return 0;

	QFile word2id("../../DKRL/word2id.txt");
	if (!word2id.open(QIODevice::WriteOnly))
		return 0;

	QFile entityWords("../../DKRL/entityWords.txt");
	if (!entityWords.open(QIODevice::WriteOnly))
		return 0;

	QFile cate("../../DKRL/ontology_class.lst");
	if (!cate.open(QIODevice::ReadOnly))
		return 0;

	QFile stop("../../DKRL/stopwords.txt");
	if (!stop.open(QIODevice::ReadOnly))
		return 0;

	QFile wordFreq("../../DKRL/wordFreq.txt");
	if (!wordFreq.open(QIODevice::WriteOnly))
		return 0;

	/*
	auto read_prot = [&](QString fname) {
		QFile fin(fname);
		if (!fin.open(QIODevice::ReadOnly))
			return 0;

		QFile protfunc("../../DKRL/prot_func.txt");
		if (!protfunc.open(QIODevice::WriteOnly))
			return 0;

		QXmlStreamReader rowl(&fin);

		QList<QString> cur_id;
		while (!rowl.atEnd()) {
			QXmlStreamReader::TokenType type = rowl.readNext();

			if (type == QXmlStreamReader::StartElement) {
				QString name = rowl.qualifiedName().toString();
				if (name == "entry") {
					cur_id.clear();
				}

				if (name == "accession") {
					rowl.readNext();
					cur_id.append(rowl.text().toString());
				}

				if (name == "comment" && !cur_id.isEmpty()) {
					if (rowl.attributes().hasAttribute("type"))
						if (rowl.attributes().value("type") == "function") {
							auto type = rowl.readNext();
							type = rowl.readNext();
							type = rowl.readNext();
							for (QString id : cur_id) {
								prot_func.insert(id, rowl.text().toString());
								protfunc.write((id + ' ' + prot_func[id]).toLatin1() + "\n");
							}
						}
				}
			}
		}

		if (rowl.hasError()) {
			qDebug() << QString::fromLocal8Bit("%1: %2, %3, %4").arg(rowl.errorString())
				.arg(rowl.lineNumber()).arg(rowl.columnNumber()).arg(rowl.characterOffset());
		}
	};

	read_prot("../../DKRL/uniprot_sprot.xml");
	*/

	QFile protfunc("../../DKRL/prot_func.txt");
	if (!protfunc.open(QIODevice::ReadOnly))
		return 0;

	while (!protfunc.atEnd()) {
		QString line = protfunc.readLine().trimmed();
		int idx = line.indexOf(' ');
		if (idx > 0) {
			prot_func.insert(line.left(idx), line.mid(idx + 1));
		}
	}

	auto write_sent = [](QFile &fout, const QString &f, const QString &p1, const QString &p2) {
		fout.write(f.arg("<" + p1 + ">").arg(p2).toLatin1() + "\n");
	};

	while (!stop.atEnd()) {
		QString word = stop.readLine().trimmed();
		stopwords.insert(word);
	}

	auto read_owl = [&](QString fname) {
		QFile fin(fname);
		if (!fin.open(QIODevice::ReadOnly))
			return 0;

		QXmlStreamReader rowl(&fin);
		QSet<QString> functions;

		QString cur_id, prop_name, cur_prop;
		int deep = 0, cur_deep;
		while (!rowl.atEnd()) {
			QXmlStreamReader::TokenType type = rowl.readNext();

			if (type == QXmlStreamReader::StartElement) {
				deep++;
				QString name = rowl.qualifiedName().toString();
				if (name == "owl:Class") {
					if (rowl.attributes().hasAttribute("rdf:about")) {
						cur_id = rowl.attributes().value("rdf:about").toString();
						cur_deep = deep;
						entities.insert("<" + cur_id + ">");
					}
				}
				if (name == "owl:ObjectProperty") {
					if (rowl.attributes().hasAttribute("rdf:about")) {
						cur_prop = rowl.attributes().value("rdf:about").toString();
						cur_deep = deep;
					}
				}
				if (name == "oboInOwl:id" && !cur_prop.isEmpty()) {
					rowl.readNext();
					QString name = rowl.text().toString();
					prop_names.insert(cur_prop, "<" + name + ">");
					relations.insert("<" + name + ">");
				}
				if (name == "rdfs:subClassOf" && !cur_id.isEmpty()) {
					if (rowl.attributes().hasAttribute("rdf:resource"))
						write_sent(train, "%1\t%2\t<is_a>", cur_id, "<"
							+ rowl.attributes().value("rdf:resource").toString() + ">");
				}
				if (name == "owl:onProperty" && !cur_id.isEmpty()) {
					if (rowl.attributes().hasAttribute("rdf:resource"))
						prop_name = rowl.attributes().value("rdf:resource").toString();
				}
				if (name == "owl:someValuesFrom" && !cur_id.isEmpty() && !prop_name.isEmpty()) {
					if (rowl.attributes().hasAttribute("rdf:resource")) {
						if (prop_names.contains(prop_name))
							prop_name = prop_names[prop_name];
						train.write(("<" + cur_id + ">\t<"
							+ rowl.attributes().value("rdf:resource") + ">\t" + prop_name).toLatin1() + "\n");
						prop_name.clear();
					}
				}

				if (name == "obo:IAO_0000115" && !cur_id.isEmpty()) {
					rowl.readNext();
					if (!functions.contains(cur_id)) {
						functions.insert(cur_id);
						QStringList lwords = remove_punc(rowl.text().toString()).split(' ', QString::SkipEmptyParts);
						entity_desc.insert("<" + cur_id + ">", lwords);
					}
				}
			}

			if (type == QXmlStreamReader::EndElement) {
				if (cur_deep == deep) {
					cur_id.clear();
					prop_name.clear();
					cur_prop.clear();
				}
				deep--;
			}
		}

		if (rowl.hasError()) {
			qDebug() << QString::fromLocal8Bit("%1: %2, %3, %4").arg(rowl.errorString())
				.arg(rowl.lineNumber()).arg(rowl.columnNumber()).arg(rowl.characterOffset());
		}
	};

	relations.insert("<is_a>");
	read_owl("../../DKRL/go.owl");

	while (!cate.atEnd()) {
		QList<QByteArray> args = cate.readLine().trimmed().split(' ');
		if (args.size() == 2)
			cates.insert("<" + args[0] + ">", args[1]);
	}

	auto read_gaf = [&](QString fname) {
		QFile fin(fname);
		if (!fin.open(QIODevice::ReadOnly))
			return 0;

		QFile dpro("../../DKRL/d_proteins_yeast.txt");
		if (!dpro.open(QIODevice::ReadOnly))
			return 0;

		QSet<QString> dpros;
		while (!dpro.atEnd())
			dpros.insert(dpro.readLine().trimmed());

		while (!fin.atEnd()) {
			QStringList args = QString(fin.readLine().trimmed()).split('\t');
			if (args[6] != "IEA" && args[6] != "ND") {
				QString uri = "http://purl.obolibrary.org/obo/" + args[4].replace(':', '_');
				QString uri_quote = "<" + uri + ">";

				if (prot_func.contains(args[1]) /*&& !entities.contains("<" + args[2] + ">")*/) {
					QStringList lwords = remove_punc(prot_func[args[1]]).split(' ', QString::SkipEmptyParts);
					entity_desc.insert("<" + args[2] + ">", lwords);

					if (!annotations.contains({ args[2], uri })) {
						annotations.insert({ args[2], uri });

						if (!dpros.contains(args[1])) {
							QString cate = cates[uri_quote];
							if (cate == "biological_process")
								train.write(("<" + args[2] + ">\t" + uri_quote + "\t<is_involved_in>").toLatin1() + "\n");
							else if (cate == "molecular_function")
								train.write(("<" + args[2] + ">\t" + uri_quote + "\t<has>").toLatin1() + "\n");
							else if (cate == "cellular_component")
								train.write(("<" + args[2] + ">\t" + uri_quote + "\t<is_located_within>").toLatin1() + "\n");
							else train.write(("<" + args[2] + ">\t" + uri_quote + "\t<is_annotated_with>").toLatin1() + "\n");
						}
					}

					entities.insert("<" + args[2] + ">");
				}
			}
		}
	};

	relations.insert("<is_involved_in>");
	relations.insert("<has>");
	relations.insert("<is_located_within>");
	relations.insert("<is_annotated_with>");
	//read_gaf("../../DKRL/goa_human.gaf");
	read_gaf("../../DKRL/goa_yeast.gaf");

	int cnt = 0;
	for (QString entity : entities) {
		entity2id.write((entity + '\t' + QString::number(cnt++)).toLatin1() + "\n");
	}

	cnt = 0;
	for (QString relation : relations) {
		relation2id.write((relation + '\t' + QString::number(cnt++)).toLatin1() + "\n");
	}

	QMapIterator<QString, QStringList> iter1(entity_desc);
	while (iter1.hasNext()) {
		for (QString word : iter1.next().value())
			words[word]++;
	}

	cnt = 0;
	for (QString word : words.keys()) {
		if (word.length() > 1 && words[word] > 1 && !stopwords.contains(word))
			word2id.write((word + '\t' + QString::number(cnt++)).toLatin1() + "\n");
		wordFreq.write((word + '\t' + QString::number(words[word])).toLatin1() + "\n");
	}

	QMapIterator<QString, QStringList> iter(entity_desc);
	while (iter.hasNext()) {
		auto item = iter.next();
		entityWords.write(item.key().toLatin1() + "\t");
		QStringList desc;
		for (QString word : item.value()) {
			if (word.length() > 1 && words[word] > 1 && !stopwords.contains(word))
				desc.append(word);
		}
		entityWords.write(QString::number(desc.size()).toLatin1() + "\t");
		for (QString word : desc)
			entityWords.write(word.toLatin1() + " ");
		entityWords.write("\n");
	}

	return 0;
}
