#include <QtCore>

#define MOUSE
#define ZEROSHOT

QMap<QString, QString> prop_names;
QSet<QString> entities, relations, dents;
QMap<QString, QStringList> entity_desc;
QMap<QString, int> words;
QSet<QString> stopwords;

QString remove_punc(QString Str) {
	Str = Str.toLower().trimmed();
	for (QChar& c : Str) {
		if (!(c >= 'a' && c <= 'z'))
			c = ' ';
	}
	return Str;
}

int main(int argc, char* argv[]) {
	QCoreApplication a(argc, argv);

#ifdef MOUSE
	QFile train("../../DKRL-gd/train.txt");
	if (!train.open(QIODevice::WriteOnly))
		return 0;

	QFile entity2id("../../DKRL-gd/entity2id.txt");
	if (!entity2id.open(QIODevice::WriteOnly))
		return 0;

	QFile relation2id("../../DKRL-gd/relation2id.txt");
	if (!relation2id.open(QIODevice::WriteOnly))
		return 0;

	QFile word2id("../../DKRL-gd/word2id.txt");
	if (!word2id.open(QIODevice::WriteOnly))
		return 0;

	QFile entityWords("../../DKRL-gd/entityWords.txt");
	if (!entityWords.open(QIODevice::WriteOnly))
		return 0;

	QFile wordFreq("../../DKRL-gd/wordFreq.txt");
	if (!wordFreq.open(QIODevice::WriteOnly))
		return 0;

	QFile dEnts("../../DKRL-gd/d_entities.txt");
	if (!dEnts.open(QIODevice::ReadOnly))
		return 0;
#else
	QFile train("../../DKRL-gd/human/train.txt");
	if (!train.open(QIODevice::WriteOnly))
		return 0;

	QFile entity2id("../../DKRL-gd/human/entity2id.txt");
	if (!entity2id.open(QIODevice::WriteOnly))
		return 0;

	QFile relation2id("../../DKRL-gd/human/relation2id.txt");
	if (!relation2id.open(QIODevice::WriteOnly))
		return 0;

	QFile word2id("../../DKRL-gd/human/word2id.txt");
	if (!word2id.open(QIODevice::WriteOnly))
		return 0;

	QFile entityWords("../../DKRL-gd/human/entityWords.txt");
	if (!entityWords.open(QIODevice::WriteOnly))
		return 0;

	QFile wordFreq("../../DKRL-gd/human/wordFreq.txt");
	if (!wordFreq.open(QIODevice::WriteOnly))
		return 0;

	QFile dEnts("../../DKRL-gd/human/d_entities.txt");
	if (!dEnts.open(QIODevice::ReadOnly))
		return 0;
#endif

	QFile stop("../../DKRL/stopwords.txt");
	if (!stop.open(QIODevice::ReadOnly))
		return 0;

	while (!stop.atEnd()) {
		QString word = stop.readLine().trimmed();
		stopwords.insert(word);
	}

#ifdef ZEROSHOT
	while (!dEnts.atEnd()) {
		dents.insert(dEnts.readLine().trimmed());
	}
#endif

	auto get_gene_desc = [&](QString fname) {
		QFile fin(fname);
		if (!fin.open(QIODevice::ReadOnly))
			return 0;

		while (!fin.atEnd()) {
			QStringList args = QString(fin.readLine().trimmed()).split('\t');
			if (args[2] != "No description available") {
				entity_desc[args[0]] = remove_punc(args[2]).split(' ', QString::SkipEmptyParts);
			}
		}
	};
#ifdef MOUSE
	get_gene_desc("../../DKRL-gd/MGI_gene_desc_latest.tsv");
#else
	get_gene_desc("../../DKRL-gd/HUMAN_gene_desc_latest.tsv");
#endif

	auto get_omim_desc = [&](QString fname) {
		QFile fin(fname);
		if (!fin.open(QIODevice::ReadOnly))
			return 0;

		QXmlStreamReader rowl(&fin);

		QString cur_id;
		QStringList cur_desc;
		int deep = 0, cur_deep = 0;
		while (!rowl.atEnd()) {
			QXmlStreamReader::TokenType type = rowl.readNext();

			if (type == QXmlStreamReader::StartElement) {
				deep++;
				QString name = rowl.qualifiedName().toString();
				if (name == "owl:Class") {
					if (rowl.attributes().hasAttribute("rdf:about")) {
						cur_id = rowl.attributes().value("rdf:about").toString();
						cur_deep = deep;
					}
				}

				if (name == "obo:IAO_0000115" && !cur_id.isEmpty()) {
					rowl.readNext();
					cur_desc = remove_punc(rowl.text().toString()).split(' ', QString::SkipEmptyParts);
				}

				if (name == "oboInOwl:hasDbXref" && !cur_id.isEmpty()) {
					rowl.readNext();
					QString omim = rowl.text().toString();
					if (omim.startsWith("OMIM:"))
						entity_desc[omim] = cur_desc;
				}
			}

			if (type == QXmlStreamReader::EndElement) {
				if (cur_deep == deep) {
					cur_id.clear();
				}
				deep--;
			}
		}

		if (rowl.hasError()) {
			qDebug() << QString::fromLocal8Bit("%1: %2, %3, %4").arg(rowl.errorString())
				.arg(rowl.lineNumber()).arg(rowl.columnNumber()).arg(rowl.characterOffset());
		}
	};
	get_omim_desc("../../DKRL-gd/doid.owl");

	auto write_sent = [](QFile& fout, const QString& f, const QString& p1, const QString& p2) {
		fout.write(f.arg("<" + p1 + ">").arg(p2).toLatin1() + "\n");
	};

	auto read_owl = [&](QString fname) {
		QFile fin(fname);
		if (!fin.open(QIODevice::ReadOnly))
			return 0;

		QXmlStreamReader rowl(&fin);
		QSet<QString> functions;

		QString cur_id, prop_name, cur_prop;
		int deep = 0, cur_deep;
		while (!rowl.atEnd()) {
			QXmlStreamReader::TokenType type = rowl.readNext();

			if (type == QXmlStreamReader::StartElement) {
				deep++;
				QString name = rowl.qualifiedName().toString();
				if (name == "owl:Class") {
					if (rowl.attributes().hasAttribute("rdf:about")) {
						cur_id = rowl.attributes().value("rdf:about").toString();
						cur_deep = deep;
						entities.insert("<" + cur_id + ">");
					}
				}
				if (name == "rdfs:subClassOf" && !cur_id.isEmpty()) {
					if (rowl.attributes().hasAttribute("rdf:resource")) {
						QString entity = rowl.attributes().value("rdf:resource").toString();
						if (entity != "http://www.w3.org/2002/07/owl#Thing") {
							write_sent(train, "%1\t%2\t<is_a>", cur_id, "<"
								+ entity + ">");
						}
					}
				}
				if (name == "owl:equivalentClass" && !cur_id.isEmpty()) {
					if (rowl.attributes().hasAttribute("rdf:resource")) {
						QString entity = rowl.attributes().value("rdf:resource").toString();
						if (entity != "http://www.w3.org/2002/07/owl#Thing") {
							write_sent(train, "%1\t%2\t<equivalent_to>", cur_id, "<"
								+ entity + ">");
						}
					}
				}
				if (name == "owl:onProperty" && !cur_id.isEmpty()) {
					if (rowl.attributes().hasAttribute("rdf:resource"))
						prop_name = rowl.attributes().value("rdf:resource").toString();
				}
				if (name == "rdf:Description" && !cur_id.isEmpty() && !prop_name.isEmpty()) {
					if (rowl.attributes().hasAttribute("rdf:about")) {
						if (prop_names.contains(prop_name)) {
							prop_name = prop_names[prop_name];
							QString entity = rowl.attributes().value("rdf:about").toString();
							if (entity != "http://www.w3.org/2002/07/owl#Thing") {
								train.write(("<" + cur_id + ">\t<"
									+ entity + ">\t" + prop_name).toLatin1() + "\n");
							}
						}
						prop_name.clear();
					}
				}
				if (name == "owl:someValuesFrom" && !cur_id.isEmpty() && !prop_name.isEmpty()) {
					if (rowl.attributes().hasAttribute("rdf:resource")) {
						if (prop_names.contains(prop_name)) {
							prop_name = prop_names[prop_name];
							QString entity = rowl.attributes().value("rdf:resource").toString();
							if (entity != "http://www.w3.org/2002/07/owl#Thing") {
								train.write(("<" + cur_id + ">\t<"
									+ rowl.attributes().value("rdf:resource") + ">\t" + prop_name).toLatin1() + "\n");
							}
						}
						prop_name.clear();
					}
				}

				if ((name == "obo:IAO_0000115" || name == "obo1:IAO_0000115") && !cur_id.isEmpty()) {
					rowl.readNext();
					if (!functions.contains(cur_id)) {
						functions.insert(cur_id);
						QStringList lwords = remove_punc(rowl.text().toString()).split(' ', QString::SkipEmptyParts);
						entity_desc.insert("<" + cur_id + ">", lwords);
					}
				}
			}

			if (type == QXmlStreamReader::EndElement) {
				if (cur_deep == deep) {
					cur_id.clear();
					prop_name.clear();
					cur_prop.clear();
				}
				deep--;
			}
		}

		if (rowl.hasError()) {
			qDebug() << QString::fromLocal8Bit("%1: %2, %3, %4").arg(rowl.errorString())
				.arg(rowl.lineNumber()).arg(rowl.columnNumber()).arg(rowl.characterOffset());
		}
	};

	relations.insert("<is_a>");
	relations.insert("<equivalent_to>");
	relations.insert("<has_quality>");
	relations.insert("<has_modifier>");
	relations.insert("<has_entity>");
	relations.insert("<has_phenotype>");
	prop_names.insert("http://aber-owl.net/#has-quality", "<has_quality>");
	prop_names.insert("http://aber-owl.net/#has-modifier", "<has_modifier>");
	prop_names.insert("http://purl.obolibrary.org/obo/BFO_0000051", "<has_entity>");
	read_owl("../../OPA2Vec-gd/phenomenet.owl");
#ifdef MOUSE
	read_owl("../../OPA2Vec-gd/mpath.owl");
	read_owl("../../OPA2Vec-gd/chebi.owl");
#endif

	auto read_hpo = [&](QString fname) {
		QFile fin(fname);
		if (!fin.open(QIODevice::ReadOnly))
			return 0;

		QSet<QPair<QString, QString>> hpairs;

		while (!fin.atEnd()) {
			QStringList args = QString(fin.readLine().trimmed()).split('\t');
			if (args[0] == "OMIM") {
				QString disease = "OMIM:" + args[1];
				QString hp = "<http://purl.obolibrary.org/obo/" + args[4].replace(':', '_') + ">";

				if (entities.contains(hp) && !hpairs.contains({ disease, hp }) && entity_desc.contains(disease)) {
					if (!dents.contains(disease))
						train.write((disease + "\t" + hp + "\t<has_phenotype>").toLatin1() + "\n");
					hpairs.insert({ disease, hp });
					entities.insert(disease);
				}
			}
		}
	};
	read_hpo("../../OPA2Vec-gd/phenotype_annotation_hpoteam.tab");

#ifdef MOUSE
	auto read_mgi = [&](QString fname) {
		QFile fin(fname);
		if (!fin.open(QIODevice::ReadOnly))
			return 0;

		QSet<QPair<QString, QString>> mpairs;

		while (!fin.atEnd()) {
			QStringList args = QString(fin.readLine().trimmed()).split('\t');
			QString mp = "<http://purl.obolibrary.org/obo/" + args[3].replace(':', '_') + ">";

			if (entities.contains(mp)) {
				QStringList genelist = args[5].split(',');
				for (QString gene : genelist) {
					if (!mpairs.contains({ gene, mp }) && entity_desc.contains(gene)) {
						if (!dents.contains(gene))
							train.write((gene + "\t" + mp + "\t<has_phenotype>").toLatin1() + "\n");
						mpairs.insert({ gene, mp });
						entities.insert(gene);
					}
				}
			}
		}
	};
	read_mgi("../../OPA2Vec-gd/MGI_PhenoGenoMP.rpt");
#else
	auto read_human_genes = [&](QString fname) {
		QFile fin(fname);
		if (!fin.open(QIODevice::ReadOnly))
			return 0;

		QSet<QPair<QString, QString>> pairs;

		fin.readLine();
		while (!fin.atEnd()) {
			QStringList args = QString(fin.readLine().trimmed()).split('\t');
			QString gene = "HGNC:" + args[0];
			QString hp = "<http://purl.obolibrary.org/obo/" + args[3].replace(':', '_') + ">";
			if (!pairs.contains({ gene, hp }) && entity_desc.contains(gene)) {
				if (!dents.contains(gene))
					train.write((gene + "\t" + hp + "\t<has_phenotype>").toLatin1() + "\n");
				pairs.insert({ gene, hp });
				entities.insert(gene);
			}
		}
	};
	read_human_genes("../../DKRL-gd/annotation/ALL_SOURCES_FREQUENT_FEATURES_genes_to_phenotype.txt");
#endif

	int cnt = 0;
	for (QString entity : entities) {
		entity2id.write((entity + '\t' + QString::number(cnt++)).toLatin1() + "\n");
	}

	cnt = 0;
	for (QString relation : relations) {
		relation2id.write((relation + '\t' + QString::number(cnt++)).toLatin1() + "\n");
	}

	QMapIterator<QString, QStringList> iter1(entity_desc);
	while (iter1.hasNext()) {
		for (QString word : iter1.next().value())
			words[word]++;
	}

	cnt = 0;
	for (QString word : words.keys()) {
		if (word.length() > 1 && words[word] > 1 && !stopwords.contains(word))
			word2id.write((word + '\t' + QString::number(cnt++)).toLatin1() + "\n");
		wordFreq.write((word + '\t' + QString::number(words[word])).toLatin1() + "\n");
	}

	QMapIterator<QString, QStringList> iter(entity_desc);
	while (iter.hasNext()) {
		auto item = iter.next();
		entityWords.write(item.key().toLatin1() + "\t");
		QStringList desc;
		for (QString word : item.value()) {
			if (word.length() > 1 && words[word] > 1 && !stopwords.contains(word))
				desc.append(word);
		}
		entityWords.write(QString::number(desc.size()).toLatin1() + "\t");
		for (QString word : desc)
			entityWords.write(word.toLatin1() + " ");
		entityWords.write("\n");
	}

	return 0;
}
