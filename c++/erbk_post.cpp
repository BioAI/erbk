#include <QtCore>

int main(int argc, char *argv[]) {
	QCoreApplication a(argc, argv);

	QFile entity2id("../../DKRL-gd/entity2id.txt");
	if (!entity2id.open(QIODevice::ReadOnly))
		return 0;

	QFile entity2vec("../../DKRL-gd/erbk/entity2vec.bern44");
	if (!entity2vec.open(QIODevice::ReadOnly))
		return 0;

	QFile ppivec("../../py/gd/gdvec_erbk.txt");
	if (!ppivec.open(QIODevice::WriteOnly))
		return 0;

	while (!entity2id.atEnd()) {
		QStringList entity = QString(entity2id.readLine().trimmed()).split('\t');
		QStringList vector = QString(entity2vec.readLine().trimmed()).split('\t');

		if (entity[0].indexOf("http://") < 0) {
			// entity[0] = entity[0].mid(1, entity[0].length() - 2);

			ppivec.write(entity[0].toLatin1());
			for (const QString& str : vector)
				ppivec.write((' ' + str).toLatin1());
			ppivec.write("\n");
		}
	}

	return 0;
}
