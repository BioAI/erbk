#include <QtCore>

QSet<QString> proteins;
QVector<QString> proteinslist;
QMap<QString, QString> idmap;

QSet<QPair<QString, QString>> posps;
QVector<QPair<QString, QString>> poslist;

QSet<QPair<QString, QString>> trainps;
QVector<QPair<QString, QString>> trainlist;
QSet<QString> trainproteins;
QVector<QString> trainproteinslist;

QSet<QPair<QString, QString>> testps[3];
QVector<QPair<QString, QString>> testlist[3];

QRandomGenerator rnd;

int main(int argc, char *argv[]) {
	QCoreApplication a(argc, argv);

	QFile link("..\\..\\OPA2Vec\\human_protein_links_valid.lst");
	if (!link.open(QIODevice::ReadOnly))
		return 0;

	QFile map("..\\..\\OPA2Vec\\9606.protein.info.v11.0.txt");
	if (!map.open(QIODevice::ReadOnly))
		return 0;

	QFile prot("..\\..\\py\\protein_with_function.txt");
	if (!prot.open(QIODevice::ReadOnly))
		return 0;

	int lcnt = 0;
	while (!map.atEnd()) {
		QByteArray line = map.readLine();
		line.chop(1);
		QList<QByteArray> args = line.split('\t');
		idmap.insert(args[0], args[1]);

		lcnt++;
	}

	while (!prot.atEnd()) {
		proteins.insert(prot.readLine().chopped(1));
	}

	for (auto val : proteins)
		proteinslist.push_back(val);

	while (!link.atEnd()) {
		QByteArray line = link.readLine();
		line.chop(1);
		QList<QByteArray> args = line.split(' ');

		if (args[2].toInt() > 0) {
			QString id1 = idmap[QString(args[0])];
			QString id2 = idmap[QString(args[1])];

			if (proteins.contains(id1) && proteins.contains(id2)) {
				posps.insert({ id1, id2 });
			}
		}
	}

	for (auto val : posps)
		poslist.push_back(val);

	for (int i = 0; i < 50500; i++) {
		auto p = poslist[rnd.bounded(poslist.size())];
		while (trainps.contains(p))
			p = poslist[rnd.bounded(poslist.size())];
		trainps.insert(p);
		trainlist.push_back(p);
		trainproteins.insert(p.first);
		trainproteins.insert(p.second);
	}

	for (auto val : trainproteins)
		trainproteinslist.push_back(val);

	for (int i = 0; i < 50500; i++) {
		QPair<QString, QString> p = { trainproteinslist[rnd.bounded(trainproteins.size())],
			trainproteinslist[rnd.bounded(trainproteins.size())] };
		while (posps.contains(p) || trainps.contains(p)) {
			p = { trainproteinslist[rnd.bounded(trainproteins.size())],
				trainproteinslist[rnd.bounded(trainproteins.size())] };
		}
		trainps.insert(p);
		trainlist.push_back(p);
		trainproteins.insert(p.first);
		trainproteins.insert(p.second);
	}

	for (int i = 0; i < 500; i++) {
		auto p = poslist[rnd.bounded(poslist.size())];
		while (trainps.contains(p) || testps[0].contains(p) ||
			!trainproteins.contains(p.first) || !trainproteins.contains(p.second))
			p = poslist[rnd.bounded(poslist.size())];
		testps[0].insert(p);
		testlist[0].push_back(p);
	}

	for (int i = 0; i < 500; i++) {
		QPair<QString, QString> p = { proteinslist[rnd.bounded(proteins.size())],
			proteinslist[rnd.bounded(proteins.size())] };
		while (posps.contains(p) || trainps.contains(p) || testps[0].contains(p) ||
			!trainproteins.contains(p.first) || !trainproteins.contains(p.second)) {
			p = { proteinslist[rnd.bounded(proteins.size())],
				proteinslist[rnd.bounded(proteins.size())] };
		}
		testps[0].insert(p);
		testlist[0].push_back(p);
	}

	for (int i = 0; i < 500; i++) {
		auto p = poslist[rnd.bounded(poslist.size())];
		while (trainps.contains(p) || testps[1].contains(p) ||
			trainproteins.contains(p.first) == trainproteins.contains(p.second))
			p = poslist[rnd.bounded(poslist.size())];
		testps[1].insert(p);
		testlist[1].push_back(p);
	}

	for (int i = 0; i < 500; i++) {
		QPair<QString, QString> p = { proteinslist[rnd.bounded(proteins.size())],
			proteinslist[rnd.bounded(proteins.size())] };
		while (posps.contains(p) || trainps.contains(p) || testps[1].contains(p) ||
			trainproteins.contains(p.first) == trainproteins.contains(p.second)) {
			p = { proteinslist[rnd.bounded(proteins.size())],
				proteinslist[rnd.bounded(proteins.size())] };
		}
		testps[1].insert(p);
		testlist[1].push_back(p);
	}

	for (int i = 0; i < 500; i++) {
		auto p = poslist[rnd.bounded(poslist.size())];
		while (trainps.contains(p) || testps[2].contains(p) ||
			trainproteins.contains(p.first) || trainproteins.contains(p.second))
			p = poslist[rnd.bounded(poslist.size())];
		testps[2].insert(p);
		testlist[2].push_back(p);
	}

	for (int i = 0; i < 500; i++) {
		QPair<QString, QString> p = { proteinslist[rnd.bounded(proteins.size())],
			proteinslist[rnd.bounded(proteins.size())] };
		while (posps.contains(p) || trainps.contains(p) || testps[2].contains(p) ||
			trainproteins.contains(p.first) || trainproteins.contains(p.second)) {
			p = { proteinslist[rnd.bounded(proteins.size())],
				proteinslist[rnd.bounded(proteins.size())] };
		}
		testps[2].insert(p);
		testlist[2].push_back(p);
	}

	QFile ftrain("..\\..\\py\\ppi_train.txt");
	if (!ftrain.open(QIODevice::WriteOnly))
		return 0;

	QFile fvalidate("..\\..\\py\\ppi_validate.txt");
	if (!fvalidate.open(QIODevice::WriteOnly))
		return 0;

	QFile ftest1("..\\..\\py\\ppi_test1.txt");
	if (!ftest1.open(QIODevice::WriteOnly))
		return 0;

	QFile ftest2("..\\..\\py\\ppi_test2.txt");
	if (!ftest2.open(QIODevice::WriteOnly))
		return 0;

	QFile ftest3("..\\..\\py\\ppi_test3.txt");
	if (!ftest3.open(QIODevice::WriteOnly))
		return 0;

	for (int i = 0; i < 50000; i++)
		ftrain.write((trainlist[i].first + "\t" + trainlist[i].second + "\t1").toLatin1() + "\n");
	for (int i = 50000; i < 50500; i++)
		fvalidate.write((trainlist[i].first + "\t" + trainlist[i].second + "\t1").toLatin1() + "\n");
	for (int i = 50500; i < 100500; i++)
		ftrain.write((trainlist[i].first + "\t" + trainlist[i].second + "\t0").toLatin1() + "\n");
	for (int i = 100500; i < 101000; i++)
		fvalidate.write((trainlist[i].first + "\t" + trainlist[i].second + "\t0").toLatin1() + "\n");

	for (int i = 0; i < 1000; i++) {
		ftest1.write((testlist[0][i].first + "\t" + testlist[0][i].second
			+ (i < 500 ? "\t1" : "\t0")).toLatin1() + "\n");
		ftest2.write((testlist[1][i].first + "\t" + testlist[1][i].second
			+ (i < 500 ? "\t1" : "\t0")).toLatin1() + "\n");
		ftest3.write((testlist[2][i].first + "\t" + testlist[2][i].second
			+ (i < 500 ? "\t1" : "\t0")).toLatin1() + "\n");
	}

	return 0;
}
