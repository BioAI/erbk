# ERBK

A representation model for biological entities byfusing structured axioms with unstructured texts

## Requirements

- Python 3
- VS 2019
- Qt 5

## Files

#### Python

- ppi_zeroshot.py

  Test the performance of Protein-protein Interaction experiments.

- gd_zeroshot.py

  Test the performance of Gene-disease Association experiments.

- gd_zeroshot_human.py

  Test the performance of Human Gene-disease Association experiments.

#### C++

- ppi_dkrl_gen.cpp

  Generate triplets of Protein-protein Interaction experiments

- gd_dkrl_gen.cpp

  Generate triplets of Gene-disease Association experiments.

- new_ppi_gen.cpp

  Generate testing data of Protein-protein Interaction experiments.

- gen_gd_dataset.cpp

  Generate testing data of Gene-disease Association experiments.

- erbk.cpp

  Main ERBK Model.

- erbk_pre.cpp erbk_post.cpp

  Pre and Post processing of ERBK Model.

#### Fact Triples

- GD_fact_triple.txt

  Fact triples generated from PhenomeNet Ontology and its annotations.

- PPI_fact_triple.txt

  Fact triples generated from Gene Ontology and GOA (Gene Ontology Annotation).
